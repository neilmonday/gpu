module vga_generator(                                    
  input						clk,                
  input						reset_n,                                                
  input			[11:0]	h_total,           
  input			[11:0]	h_sync,           
  input			[11:0]	h_start,             
  input			[11:0]	h_end,                                                    
  input			[11:0]	v_total,           
  input			[11:0]	v_sync,            
  input			[11:0]	v_start,           
  input			[11:0]	v_end, 
  input			[11:0]	v_active_14, 
  input			[11:0]	v_active_24, 
  input			[11:0]	v_active_34, 
  output	reg				vga_hs,             
  output	reg				vga_vs,           
  output	reg				vga_de,
  output	reg	[7:0]		vga_r,
  output	reg	[7:0]		vga_g,
  output	reg	[7:0]		vga_b                                                 
);

//=======================================================
//  Signal declarations
//=======================================================
reg	[11:0]	h_count;
reg	[3:0]		pixel_r;
reg	[3:0]		pixel_g;
reg	[3:0]		pixel_b;
reg	[11:0]	v_count;
reg				h_act; 
reg				h_act_d;
reg				v_act; 
reg				v_act_d; 
reg				pre_vga_de;
wire				h_max, hs_end, hr_start, hr_end;
wire				v_max, vs_end, vr_start, vr_end;
wire				v_act_14, v_act_24, v_act_34;
//reg				boarder;

reg	[13:0]	fb_address;
reg	[11:0]	fb_data;
reg				fb_wren;
wire	[11:0]	fb_q[5][4];

reg	[2:0]		fb_x;
reg	[2:0]		fb_y;

//=======================================================
//  Structural coding
//=======================================================
assign h_max = h_count == h_total;
assign hs_end = h_count >= h_sync;
assign hr_start = h_count == h_start; 
assign hr_end = h_count == h_end;
assign v_max = v_count == v_total;
assign vs_end = v_count >= v_sync;
assign vr_start = v_count == v_start; 
assign vr_end = v_count == v_end;
assign v_act_14 = v_count == v_active_14; 
assign v_act_24 = v_count == v_active_24; 
assign v_act_34 = v_count == v_active_34;

//640 x 480 x 12 bits = 3686400
//24 x 65536 = 1572864 
//i need 3 of these to store the whole framebuffer
/*framebuffer u_framebuffer (
	.address(fb_address),	//input	[15:0]  address;
	.data(fb_data),			//input	[23:0]  data;
	.inclock(clk),				//input	  inclock;
	.outclock(clk),			//input	  outclock;
	.wren(fb_wren),			//input	  wren;
	.q(fb_q)						//output	[23:0]  q;
);
*/

generate
	genvar i;
	genvar j;
	for (i=0; i<5; i=i+1) begin : dff
		for (j=0; j<4; j=j+1) begin : dff
			framebuffer u_framebuffer(
				.address(fb_address),	//input	[13:0]  address;
				.data(fb_data),			//input	[11:0]  data;
				.clock(clk),				//input	  clock;
				.wren(fb_wren),			//input	  wren;
				.q(fb_q[i][j])				//output	[11:0]  q;
			);
		end
	end
endgenerate

always @ (posedge clk or negedge reset_n)
	if (!reset_n)
	begin
		fb_address <= 14'b0;
	end
	else
	begin
		if (h_max && v_max)
			fb_address <= 14'b0;
				
		fb_address <= (h_count % 128) + ((v_count % 128) << 7);
	end

//horizontal control signals
always @ (posedge clk or negedge reset_n)
	if (!reset_n)
	begin
		h_act_d	<=	1'b0;
		h_count	<=	12'b0;
		pixel_r	<=	4'b0;
		pixel_g	<=	4'b0;
		pixel_b	<=	4'b0;
		vga_hs	<=	1'b1;
		h_act		<=	1'b0;
		fb_x = 3'b0;
		fb_y = 3'b0;
	end
	else
	begin
		fb_x = h_count >> 7;
		fb_y = v_count >> 7;
		
		h_act_d	<=	h_act;
		
		if (h_max)
			h_count	<=	12'b0;
		else
			h_count	<=	h_count + 12'b1;
		
		if(h_act_d)
		begin
			/*if((fb_x + fb_y) % 2)
			begin*/
				pixel_r	<=	fb_q[fb_x][fb_y][3:0];
				pixel_g	<=	fb_q[fb_x][fb_y][7:4];
				pixel_b	<=	fb_q[fb_x][fb_y][11:8];
			/*end
			else
			begin
				pixel_r	<=	h_count[9:6];
				pixel_g	<=	v_count[6:3];
				pixel_b	<=	4'b1100;
			end*/
		end

		if (hs_end && !h_max)
			vga_hs	<=	1'b1;
		else
			vga_hs	<=	1'b0;

		if (hr_start)
			h_act		<=	1'b1;
		else if (hr_end)
			h_act		<=	1'b0;
	end

//vertical control signals
always@(posedge clk or negedge reset_n)
	if(!reset_n)
	begin
		v_act_d		<=	1'b0;
		v_count		<=	12'b0;
		vga_vs		<=	1'b1;
		v_act			<=	1'b0;
	end
	else 
	begin		
		if (h_max)
		begin		  
			v_act_d	  <=	v_act;
		  
			if (v_max)
				v_count	<=	12'b0;
			else
				v_count	<=	v_count + 12'b1;

			if (vs_end && !v_max)
				vga_vs	<=	1'b1;
			else
				vga_vs	<=	1'b0;

			if (vr_start)
				v_act <=	1'b1;
			else if (vr_end)
				v_act <=	1'b0;
		end
	end

//pattern generator and display enable
always @(posedge clk or negedge reset_n)
begin
	if (!reset_n)
	begin
		vga_de		<=	1'b0;
		pre_vga_de	<=	1'b0;
		//boarder		<=	1'b0;		
	end
	else
	begin
		vga_de		<=	pre_vga_de;
		pre_vga_de	<=	v_act && h_act;
    
		/*if ((!h_act_d&&h_act) || hr_end || (!v_act_d&&v_act) || vr_end)
			boarder	<=	1'b1;
		else
			boarder	<=	1'b0;*/   		
		
		/*if (boarder)
			{vga_r, vga_g, vga_b} <= {8'hFF,8'hFF,8'hFF};
		else
		begin*/
			vga_r[7:4] <= pixel_r[3:0];
			vga_g[7:4] <= pixel_g[3:0];
			vga_b[7:4] <= pixel_b[3:0];
		//end
	end
end	

endmodule